using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {
	public Animator anim;
	public float turnValue, forwardValue;


	void Update() {
		float val = 0.5f;
		if(Input.GetKey(KeyCode.A)) {
			turnValue -= 0.02f;
		} else if(Input.GetKey(KeyCode.D)) {
			turnValue += 0.02f;
		} else
			turnValue = 0;

		val += turnValue;

		if(Input.GetKey(KeyCode.W)) {
			forwardValue += 0.02f;
		} else
			forwardValue -= 0.05f;
		turnValue = Mathf.Clamp(turnValue, -0.5f, 0.5f);
		val = Mathf.Clamp(val, 0, 1);
		forwardValue = Mathf.Clamp(forwardValue, 0, 1);

		anim.SetFloat("Blend", val);
		anim.SetFloat("Forward", forwardValue);

		if(Input.GetKeyDown(KeyCode.Space))
			anim.SetTrigger("Wave");
	}
}
